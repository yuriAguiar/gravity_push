- Description

Gravity Push is a platformer/FPS powered by the Unreal Engine 4, which gives the player the power to push and pull scenery elements to BRING GIANT TOWERS DOWN!

It was created as a PoC project with the vision of making a game that have all (if not most) of it's world elements driven by physics so that you can enjoy
physics based puzzle solving, combat, construction and destruction of the environment. The game is still a prototype project so for now you have the platforming, combat and
destruction of buildings features working.

- Installation instructions

The game has no need for installation so you just have to open the executable file to play.
To play the game you have 2 options:

1. You can download the project from GitLab. If you choose to do that you will need to have the Unreal Engine Editor 4.22 or above to open the projec.
To play you can either just hit play on the editor or generate the executable files by going to File > Package Project, selecting the operational system
you want to export to (we recomend a desktop OS such as Windows because the project wasn't made with mobile or web environments in mind) and finally selecting to wich folder
you want the exported files to go.

2. You can download the game's executables directly from it's official page on itch.io: https://gentlegiant.itch.io/gravity-push. The itch.io executables are for Windows only.

Have Fun bringing that tower down!